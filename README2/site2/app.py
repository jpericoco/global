import os
from flask import Flask
app = Flask(__name__)

@app.route('/')
def welcome():
    return "<h1 style='color:blue'>desafio2</h1>"

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=os.getenv('PORT'))
